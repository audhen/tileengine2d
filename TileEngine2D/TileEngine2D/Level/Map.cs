﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public class Map
    {
        #region Attributes
        private int width;
        private int height;
        private int tileWidth;
        private int tileHeight;
        //Beschreibung und Inhalt
        private Dictionary<String, String> metaData = new Dictionary<string, string>();
        //Kuerzel und Textur-Pfad
        private Dictionary<String, String> textureNamesAndFiles = new Dictionary<string, string>();
        private Dictionary<String, Texture2D> textureNamesAndInstances = new Dictionary<string, Texture2D>();

        //Lebende Entitäten


        //X und Y
        private Tile[,] tiles;
        private ContentManager content;
        #endregion


        #region Constructors
        public Map(String file, ContentManager content)
        {
            this.content = content;

            //Map laden
            if (File.Exists(file))
            {
                this.Load(file);
            }
            else
            {
                throw new ExceptionMapFileError("File does not exist. " + file);
            }
        }

        public Map(ContentManager content)
        {
            Width = 64;
            Height = 32;
            tileWidth = 32;
            TileHeight = 32;
            textureNamesAndFiles.Add("error", "Textures/Debug/Error");
            textureNamesAndFiles.Add("grass", "Textures/Tiles/Grass");
            textureNamesAndFiles.Add("mud", "Textures/Tiles/Mud");
            textureNamesAndFiles.Add("plant", "Textures/Tiles/GrassPlant");

            //Texturen in den Speicher laden.
            foreach (KeyValuePair<String, String> p in textureNamesAndFiles)
            {
                textureNamesAndInstances.Add(p.Key, content.Load<Texture2D>(p.Value));
            }

            Tiles = new Tile[Width, Height];
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Tiles[x, y] = new Tile("error", false, true);
                    if (y % 2 == 0)
                    {
                        Tiles[x, y].Textures.Add("grass");
                    }
                    else
                    {
                        Tiles[x, y].Textures.Add("plant");
                    }

                }
            }
            Tiles[5, 5].Textures.Add("mud");
            Tiles[5, 5].Collidable = true;


        }
        #endregion


        #region Methods

        /// <summary>
        /// Zeichnet alle Tiles, unabhaengig ob sichtbar oder nicht.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (Tiles[x, y].Visible)
                    {
                        //Zeichne jede Textur der Tile
                        foreach (String textureName in Tiles[x, y].Textures)
                        {
                            spriteBatch.Draw(textureNamesAndInstances[textureName], new Vector2(x * TileWidth, y * TileHeight), Color.White);
                        }
                        if (Engine.DEBUGMODE && Tiles[x, y].Collidable) spriteBatch.Draw(textureNamesAndInstances["error"], new Vector2(x * TileWidth, y * TileHeight), Color.White * .4f);
                    }
                }
            }
        }

        /// <summary>
        /// Zeichnet nur die Tiles, welche sich innerhalb der Region befinden.
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="region"></param>
        public void Draw(SpriteBatch spriteBatch, Rectangle region)
        {
            int yMin = (int)Math.Floor((float)region.Y / TileHeight);
            int yMax = (int)Math.Floor((float)(region.Y + region.Height) / TileHeight);
            int xMin = (int)Math.Floor((float)region.X / TileWidth);
            int xMax = (int)Math.Floor((float)(region.X + region.Width) / TileWidth);

            if (yMax >= Height) yMax = Height - 1;
            if (xMax >= Width) xMax = Width - 1;

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    if (Tiles[x, y].Visible)
                    {
                        //Zeichne jede Textur der Tile
                        foreach (String textureName in Tiles[x, y].Textures)
                        {
                            spriteBatch.Draw(textureNamesAndInstances[textureName], new Vector2(x * TileWidth, y * TileHeight), Color.White);
   
                        }
                        if (Engine.DEBUGMODE && Tiles[x, y].Collidable) spriteBatch.Draw(textureNamesAndInstances["error"], new Vector2(x * TileWidth, y * TileHeight), Color.White * .4f);
                    }
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            //TODO
        }

        private void Load(String file)
        {
            try
            {
                XmlDocument mapDoc = new XmlDocument();
                mapDoc.Load(file);

                XmlNode metadata = mapDoc.SelectSingleNode("//Map/MetaData");
                foreach (XmlAttribute attr in metadata.Attributes)
                {
                    String name = (String)attr.Name;
                    String value = (String)attr.Value;
                    switch (name)
                    {
                        case "Width": Width = int.Parse(value); break;
                        case "Height": Height = int.Parse(value); break;
                        case "TileWidth": TileWidth = int.Parse(value); break;
                        case "TileHeight": TileHeight = int.Parse(value); break;
                        default:
                            MetaData.Add(name, value); break;
                    }
                }
                Tiles = new Tile[Width, Height];

                XmlNode textures = mapDoc.SelectSingleNode("//Map/Textures");
                foreach (XmlAttribute attr in textures.Attributes)
                {
                    textureNamesAndFiles.Add((String)attr.Name, (String)attr.Value);
                }

                XmlNode tiles = mapDoc.SelectSingleNode("//Map/Tiles");
                foreach (XmlNode t in tiles.ChildNodes)
                {
                    //Vorbelegung
                    int x = -1;
                    int y = -1;
                    bool collidable = false;
                    bool visible = false;

                    //Position und Zustand auslesen
                    foreach (XmlAttribute attr in t.Attributes)
                    {
                        String name = (String)attr.Name;
                        String value = (String)attr.Value;
                        switch (name)
                        {
                            case "x":
                                x = int.Parse(value);
                                break;
                            case "y":
                                y = int.Parse(value);
                                break;
                            case "collidable":
                                collidable = bool.Parse(value);
                                break;
                            case "visible":
                                visible = bool.Parse(value);
                                break;
                        }
                    }

                    //Texturen auslesen und ggf. hinzufuegen.
                    List<String> textureNames = new List<String>();
                    foreach (XmlAttribute attr in t.FirstChild.Attributes)
                    {
                        String name = (String)attr.Name;
                        String value = (String)attr.Value;
                        textureNames.Add(name);
                        //Falls die Textur noch nicht bekannt ist, fuege Sie hinzu.
                        if (!textureNamesAndFiles.ContainsKey(name))
                        {
                            textureNamesAndFiles.Add(name, value);
                        }
                    }

                    //Tile erstellen und einordnen.
                    Tiles[x, y] = new Tile(textureNames, collidable, visible);
                }

                //Texturen in den Speicher laden.
                foreach (KeyValuePair<String, String> p in textureNamesAndFiles)
                {
                    textureNamesAndInstances.Add(p.Key, content.Load<Texture2D>(p.Value));
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Save(String fileName)
        {
            if (File.Exists(fileName))
            { File.Delete(fileName); }

            XmlDocument mapFile = new XmlDocument();

            XmlNode myRoot = mapFile.CreateElement("Map");
            mapFile.AppendChild(myRoot);

            //Map/MetaData
            XmlNode metaData = mapFile.CreateElement("MetaData");
            AppendAttribute(mapFile, "Width", Width, metaData);
            AppendAttribute(mapFile, "Height", Height, metaData);
            AppendAttribute(mapFile, "TileWidth", TileWidth, metaData);
            AppendAttribute(mapFile, "TileHeight", TileHeight, metaData);
            foreach (KeyValuePair<String, String> pair in MetaData)
            {
                AppendAttribute(mapFile, pair.Key, pair.Value, metaData);
            }
            myRoot.AppendChild(metaData);

            //Map/Textures
            XmlNode textures = mapFile.CreateElement("Textures");
            foreach (KeyValuePair<String, String> pair in textureNamesAndFiles)
            {
                AppendAttribute(mapFile, pair.Key, pair.Value, textures);
            }
            myRoot.AppendChild(textures);

            //Map/Tiles
            XmlNode tiles = mapFile.CreateElement("Tiles");
            //Zeilenweise die Daten ablegen
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    //Map/Tiles/Tile
                    XmlNode tile = mapFile.CreateElement("Tile");
                    AppendAttribute(mapFile, "x", x, tile);
                    AppendAttribute(mapFile, "y", y, tile);
                    AppendAttribute(mapFile, "collidable", Tiles[x, y].Collidable, tile);
                    AppendAttribute(mapFile, "visible", Tiles[x, y].Visible, tile);

                    //Map/Tiles/Tile/Textures
                    XmlNode tileTextures = mapFile.CreateElement("Textures");
                    foreach (String s in Tiles[x, y].Textures)
                    {
                        //Speichere den Texturnamen und den bekannten Texturspeicherort.
                        AppendAttribute(mapFile, s, textureNamesAndFiles[s], tileTextures);
                    }
                    tile.AppendChild(tileTextures);

                    tiles.AppendChild(tile);
                }
            }
            myRoot.AppendChild(tiles);

            mapFile.Save(fileName);
        }



        private void AppendAttribute(XmlDocument doc, String name, Object value, XmlNode node)
        {
            XmlAttribute attr = doc.CreateAttribute(name);
            attr.Value = (String)(value + "");
            node.Attributes.Append(attr);
        }
        #endregion

        #region Properties
        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public Tile[,] Tiles
        {
            get { return tiles; }
            set { tiles = value; }
        }

        public int TileWidth
        {
            get { return tileWidth; }
            set { tileWidth = value; }
        }

        public int TileHeight
        {
            get { return tileHeight; }
            set { tileHeight = value; }
        }

        public Dictionary<String, String> MetaData
        {
            get { return metaData; }
            set { metaData = value; }
        }
        #endregion
    }

    public class ExceptionMapFileError : System.Exception
    {
        public ExceptionMapFileError() { }
        public ExceptionMapFileError(String message) : base(message) { }
        public ExceptionMapFileError(String message, System.Exception inner) : base(message, inner) { }
    }

    public class ExceptionMapLoadError : System.Exception
    {
        public ExceptionMapLoadError() { }
        public ExceptionMapLoadError(String message) : base(message) { }
        public ExceptionMapLoadError(String message, System.Exception inner) : base(message, inner) { }
    }
}
