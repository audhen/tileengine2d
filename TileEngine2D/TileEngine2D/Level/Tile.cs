﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public class Tile
    {
        #region Attributes
        List<String> textures = new List<String>();
        bool collidable;
        bool visible;
        #endregion


        #region Constructors
        public Tile(String texture, bool collidable = false, bool visible = false)
        {
            Textures.Add(texture);
            Collidable = collidable;
            Visible = visible;
        }

        public Tile(List<String> textures, bool collidable = false, bool visible = false)
        {
            Textures = textures;
            Collidable = collidable;
            Visible = visible;
        }
        #endregion

        #region Methods
        #endregion


        #region Properties
        public List<String> Textures
        {
            get { return textures; }
            set { textures = value; }
        }

        public bool Collidable
        {
            get { return collidable; }
            set { collidable = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }
        #endregion

    }
}
