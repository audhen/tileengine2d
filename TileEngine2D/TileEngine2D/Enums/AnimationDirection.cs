﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TileEngine2D
{
    public enum AnimationDirection
    {
        UP,
        UPLEFT,
        UPRIGHT,
        LEFT,
        RIGHT,
        DOWN,
        DOWNLEFT,
        DOWNRIGHT,
        NONE
    }
}
