﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public class Prop : IDrawable
    {
        private Texture2D texture;
        private Vector2 position;
        private Rectangle hitbox;

        private bool collidable;
        private bool interactible;
        

        public Prop(Vector2 position, Texture2D texture, bool collidable = true, bool interactible = false)
        {
            Position = position;
            Texture = texture;
            Collidable = collidable;
            Interactible = interactible;
            this.hitbox = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
        }


        public int ZLevel
        {
            get { return (int)Position.Y - Texture.Height; }
        }

        public void Update(GameTime gameTime)
        {
            //TODO
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, Color.White);
        }

        public void interact(Player player)
        {

        }

        #region Properties
        public Rectangle Hitbox
        {
            get { return hitbox; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public bool Collidable
        {
            get { return collidable; }
            set { collidable = value; }
        }


        public bool Interactible
        {
            get { return interactible; }
            set { interactible = value; }
        }
        #endregion

    }
}
