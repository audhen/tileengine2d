﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;


namespace TileEngine2D
{
    public class Player : IDrawable
    {
        #region Attributes
        private Engine engine;
        private Texture2D texture;
        private Vector2 position;
        private Vector2 previousPosition;

        private Rectangle myHitbox;

        private Animation walkingAnimation;
        private AnimationDirection walkingAnimationLastDirection = AnimationDirection.NONE;
        private AnimationDirection walkingAnimationCurrentDirection = AnimationDirection.NONE;

        private bool collidable;
        private bool alive;
        private bool visible;
        private bool isColliding;
        private float speed = 2.0f;
        private float speedModifier = 1f;
        private float speedTurboFactor = 4f;
        private int animationSpeed = 160;

        private float maxHealth = 100f;
        private float currentHealth = 100f;

        //private float maxArmor;
        //private float currentArmor;


        public int spriteWidth = 32;
        public int spriteHeight = 48;

        private Rectangle animRect;

        private KeyboardState previousKBState;
        private KeyboardState currentKBState;

        private const Keys KEYS_UP = Keys.W;
        private const Keys KEYS_DOWN = Keys.S;
        private const Keys KEYS_LEFT = Keys.A;
        private const Keys KEYS_RIGHT = Keys.D;
        #endregion

        #region Constructors
        public Player(ContentManager content, Engine engine, Vector2 position, bool collidable = false, bool alive = true, bool visible = true, bool isColliding = false)
        {
            this.engine = engine;
            //Sprite einlesen und WalkingAnimation initialisieren.
            Texture = content.Load<Texture2D>("Sprites/Player/steampunk_f10");
            List<AnimationDirection> walkingDirections = new List<AnimationDirection>();
            walkingDirections.Add(AnimationDirection.DOWN);
            walkingDirections.Add(AnimationDirection.LEFT);
            walkingDirections.Add(AnimationDirection.RIGHT);
            walkingDirections.Add(AnimationDirection.UP);
            walkingAnimation = new Animation(spriteWidth, spriteHeight, Texture.Width, Texture.Height, walkingDirections, animationSpeed);

            Position = position;
            previousPosition = Position;
            Collidable = collidable;
            Alive = alive;
            Visible = visible;
            IsColliding = isColliding;
        }
        #endregion

        #region Methods
        public void Update(GameTime gameTime)
        {
            currentKBState = Keyboard.GetState();

            #region Eingabe-Auswertung
            HandleInputMisc();
            HandleInputInteraction(gameTime);
            HandleInputMovement(gameTime);
            #endregion

            if (Position.X <= 0 || Position.Y <= 0 || Position.X + spriteWidth >= engine.map.Width * engine.map.TileWidth || Position.Y + spriteHeight >= engine.map.Height * engine.map.TileHeight)
            {
                Position = previousPosition;
            }

            #region Kollisionserkennung
            myHitbox = new Rectangle((int)Position.X, (int)Position.Y + spriteHeight/2, spriteWidth, spriteHeight/2);
            HandleCollisionMap();
            HandleCollisionProps();
            HandleCollisionEntities();
            #endregion

            engine.camera.Update(gameTime);
            engine.pHealthbar.CurrentHealth = CurrentHealth;

            walkingAnimationLastDirection = walkingAnimationCurrentDirection;
            this.previousPosition = position;
            this.previousKBState = this.currentKBState;
        }

        private bool collidesWithMap()
        {
            bool result = false;
            int leftTile = (int)Math.Floor((float)myHitbox.Left / engine.map.TileWidth);
            int rightTile = (int)Math.Floor((float)myHitbox.Right / engine.map.TileWidth);
            int topTile = (int)Math.Floor((float)myHitbox.Top / engine.map.TileHeight);
            int bottomTile = (int)Math.Floor((float)myHitbox.Bottom / engine.map.TileHeight);
            for(int y = topTile; y <= bottomTile; y++)
            {
                for (int x = leftTile; x <= rightTile; x++)
                {
                    if (x < engine.map.Width && y < engine.map.Height && engine.map.Tiles[x, y].Collidable) result = true;
                }
            }
            return result;
        }



        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, animRect, Color.White);
        }

        public void HandleInputInteraction(GameTime gameTime)
        {
            if (currentKBState.IsKeyDown(Keys.Enter) && !previousKBState.IsKeyDown(Keys.Enter))
            {
                foreach (Prop p in engine.Props)
                {
                    if (Vector2.Distance(Position, p.Position) <= Hitbox.Width)
                    {
                        if (p.Interactible)
                        {
                            float pHitboxCenter = Hitbox.X + (Hitbox.Width / 2);
                            if ((p.Hitbox.Y + p.Hitbox.Height) == Hitbox.Y && pHitboxCenter >= p.Hitbox.X && pHitboxCenter <= p.Hitbox.X + p.Hitbox.Width)
                            {
                                Console.WriteLine("Jo, ich wurde angeklickt: " + p.Hitbox.X + ";" + p.Hitbox.Y);
                                CurrentHealth -= 7;
                            }
                        }
                    }
                }
            }
        }


        private void HandleInputMovement(GameTime gameTime)
        {
            if (currentKBState.IsKeyDown(Keys.LeftShift))
            {
                speedModifier = speedTurboFactor;
            }
            else
            {
                speedModifier = 1f;
            }

            //Richtungsauswertung
            //Behandlung der WalkingAnimation
            if (currentKBState.IsKeyDown(KEYS_UP))
            {
                walkingAnimationCurrentDirection = Animation.KeyToDirection(KEYS_UP);
                animRect = walkingAnimation.NextAnimation(gameTime, walkingAnimationCurrentDirection, walkingAnimationLastDirection);
                position.Y = position.Y - speed * speedModifier;
            }
            else if (currentKBState.IsKeyDown(KEYS_LEFT))
            {
                walkingAnimationCurrentDirection = Animation.KeyToDirection(KEYS_LEFT);
                animRect = walkingAnimation.NextAnimation(gameTime, walkingAnimationCurrentDirection, walkingAnimationLastDirection);
                position.X = position.X - speed * speedModifier;
            }
            else if (currentKBState.IsKeyDown(KEYS_RIGHT))
            {
                walkingAnimationCurrentDirection = Animation.KeyToDirection(KEYS_RIGHT);
                animRect = walkingAnimation.NextAnimation(gameTime, walkingAnimationCurrentDirection, walkingAnimationLastDirection);
                position.X = position.X + speed * speedModifier;
            }
            else if (currentKBState.IsKeyDown(KEYS_DOWN))
            {
                walkingAnimationCurrentDirection = Animation.KeyToDirection(KEYS_DOWN);
                animRect = walkingAnimation.NextAnimation(gameTime, walkingAnimationCurrentDirection, walkingAnimationLastDirection);
                position.Y = position.Y + speed * speedModifier;
            }
            else
            {
                //Animation zurücksetzen. Spieler drückt keine Taste.
                animRect = walkingAnimation.NextAnimation(gameTime, AnimationDirection.NONE, walkingAnimationLastDirection);
            }
        }

        private void HandleCollisionProps()
        {
            //Kollisionserkennung für Props
            foreach (Prop p in engine.Props)
            {
                float distance = Vector2.Distance(Position, p.Position);
                if (distance <= 64)
                {
                    if(p.Collidable && Hitbox.Intersects(p.Hitbox)) Position = previousPosition;
                }
            }
        }

        private void HandleCollisionMap()
        {
            //Kollisionserkennung für die MapTiles
            if (this.collidesWithMap())
            {
                Position = previousPosition;
            }
        }

        private void HandleCollisionEntities()
        {
            //Kollisionserkennung für die NPCs
            foreach (Entity ent in engine.Npcs)
            {
                float distance = Vector2.Distance(Position, ent.Position);
                if (distance <= 64)
                {
                    if (Hitbox.Intersects(ent.Hitbox)) Position = previousPosition;
                }
            }
        }

        private void HandleInputMisc()
        {
            //Debug-Modus ein/ausschalten.
            if (currentKBState.IsKeyDown(Keys.F12) && !previousKBState.IsKeyDown(Keys.F12)) Engine.DEBUGMODE = !Engine.DEBUGMODE;

            //Test, Einstellung der Durchlaessigkeit der UI Elemente
            if (currentKBState.IsKeyDown(Keys.Add) && !previousKBState.IsKeyDown(Keys.Add)) engine.pHealthbar.OpacityFactor += 0.05f;
            if (currentKBState.IsKeyDown(Keys.Subtract) && !previousKBState.IsKeyDown(Keys.Subtract)) engine.pHealthbar.OpacityFactor -= 0.05f;
            

            //Test, Einstellung der Größe der UI Elemente
            if (currentKBState.IsKeyDown(Keys.NumPad7) && !previousKBState.IsKeyDown(Keys.NumPad7)) engine.pHealthbar.ScalingFactor += 0.05f;
            if (currentKBState.IsKeyDown(Keys.NumPad1) && !previousKBState.IsKeyDown(Keys.NumPad1)) engine.pHealthbar.ScalingFactor -= 0.05f;
        }
        #endregion



        #region Properties
        public Vector2 PreviousPosition
        {
            get { return previousPosition; }
            set { previousPosition = value; }
        }

        public Rectangle Hitbox
        {
            get { return myHitbox; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public bool Collidable
        {
            get { return collidable; }
            set { collidable = value; }
        }

        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public bool IsColliding
        {
            get { return isColliding; }
            set { isColliding = value; }
        }

        public int ZLevel
        {
            get { return (int)Position.Y; }
        }

        public float MaxHealth
        {
            get { return maxHealth; }
            set { maxHealth = value; }
        }


        public float CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }
        #endregion
    }
}
