﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace TileEngine2D
{
    public class SpinningEntity : Entity
    {
        #region Attributes
        private SpinningAnimation spinningAnimation;
        private Rectangle animRect;
        private int spriteWidth = 32;
        private int spriteHeight = 48;
        #endregion


        #region Constructors
        public SpinningEntity(ContentManager content, String texture, Vector2 position, bool collidable = true, bool alive = true, bool visible = true, bool isColliding = false)
            : base (content, texture, position, collidable, alive, visible, isColliding)
        {

            this.spinningAnimation = new SpinningAnimation(spriteWidth, spriteHeight, Texture.Width, Texture.Height, new List<AnimationDirection>() { AnimationDirection.DOWN, AnimationDirection.LEFT, AnimationDirection.RIGHT, AnimationDirection.UP }, 100);
            base.Hitbox = new Rectangle((int)Position.X, (int)Position.Y + spriteHeight / 2, spriteWidth, spriteHeight / 2);
        }


        #endregion

        #region Methods
        public override void Update(GameTime gameTime)
        {
            animRect = spinningAnimation.NextAnimation(gameTime, AnimationDirection.NONE, AnimationDirection.NONE);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, animRect, Color.White);
        }
        #endregion

    }
}
