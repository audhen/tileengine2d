﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace TileEngine2D
{
    public class Entity : IDrawable
    {
        #region Attributes
        private Texture2D texture;
        private Vector2 position;
        private bool collidable;
        private bool alive;
        private bool visible;
        private bool isColliding;
        private Rectangle hitbox;
        #endregion


        #region Constructors
        public Entity(ContentManager content, String texture, Vector2 position, bool collidable = true, bool alive = true, bool visible = true, bool isColliding = false)
        {
            Texture = content.Load<Texture2D>(texture);
            Position = position;
            Collidable = collidable;
            Alive = alive;
            Visible = visible;
            IsColliding = isColliding;

            this.hitbox = new Rectangle((int)Position.X, (int)Position.Y + Texture.Height / 2, Texture.Width, Texture.Height / 2);
        }


        #endregion

        #region Methods
        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, Color.White);
        }
        #endregion

        #region Properties
        public Rectangle Hitbox
        {
            get { return hitbox; }
            set { hitbox = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public bool Collidable
        {
            get { return collidable; }
            set { collidable = value; }
        }

        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public bool IsColliding
        {
            get { return isColliding; }
            set { isColliding = value; }
        }

        public int ZLevel
        {
            get { return (int)Position.Y; }
        }
        #endregion

    }
}
