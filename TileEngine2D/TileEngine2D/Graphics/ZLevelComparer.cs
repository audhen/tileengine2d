﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TileEngine2D
{
    public class ZLevelComparer : IComparer<IDrawable>
    {
        public int Compare(IDrawable x, IDrawable y)
        {
            if (x.ZLevel == y.ZLevel) return 0;
            if (x.ZLevel > y.ZLevel) return 1;
            return -1;

        }
    }
}
