﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public class Camera
    {
        private Vector2 position;
        private Player player;
        private Map map;

        public Camera(Player player, Map map)
        {
            this.player = player;
            this.map = map;
        }

        public void Update(GameTime gameTime)
        {
            float grenzDurchmesser = (Engine.CAMERA_WIDTH * 0.1f);
            float grenzeRechts = Engine.CAMERA_WIDTH - grenzDurchmesser;
            float grenzeLinks = grenzDurchmesser;
            float grenzeOben = grenzDurchmesser;
            float grenzeUnten = Engine.CAMERA_HEIGHT - grenzDurchmesser;

            if (player.Position.X >= grenzeRechts + position.X)
            {
                if (player.Position.X > player.PreviousPosition.X)
                {
                    position.X = player.Position.X - grenzeRechts;
                }
            }
            if (player.Position.X <= grenzeLinks + position.X)
            {
                if (player.Position.X < player.PreviousPosition.X)
                {
                    position.X = player.Position.X - grenzeLinks;
                }
            }
            if (player.Position.Y <= grenzeOben + position.Y)
            {
                if (player.Position.Y < player.PreviousPosition.Y)
                {
                    position.Y = player.Position.Y - grenzeOben;
                }
            }
            if (player.Position.Y >= grenzeUnten + position.Y)
            {
                if (player.Position.Y > player.PreviousPosition.Y)
                {
                    position.Y = player.Position.Y - grenzeUnten;
                }
            }


            if (position.X <= 0 || player.Position.X <= 0) position.X = 0;
            if (position.Y <= 0 || player.Position.Y <= 0) position.Y = 0;
            if (position.X >= map.Width * map.TileWidth - Engine.CAMERA_WIDTH || player.Position.X + player.spriteWidth >= map.Width * map.TileWidth) position.X = map.Width * map.TileWidth - Engine.CAMERA_WIDTH;
            if (position.Y >= map.Height * map.TileHeight - Engine.CAMERA_HEIGHT || player.Position.Y + player.spriteHeight >= map.Width * map.TileWidth) position.Y = map.Height * map.TileHeight - Engine.CAMERA_HEIGHT;

            Engine.CAMERA_MATRIX = Matrix.CreateTranslation(new Vector3(-Position, 0));
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }


    }
}
