﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TileEngine2D
{
    public class Animation : IAnimation
    {
        private int spriteWidth;
        private int spriteHeight;
        private int textureWidth;
        private int textureHeight;

        private int animSpeed;
        private int animFrame = 0;
        private double lastAnimTime = 0;

        private Rectangle animationRectangle = new Rectangle();

        private Dictionary<AnimationDirection, List<Rectangle>> spriteRectangles = new Dictionary<AnimationDirection, List<Rectangle>>();

        public Animation(int spriteWidth, int spriteHeight, int textureWidth, int textureHeight, List<AnimationDirection> directions, int animSpeed = 160)
        {
            this.spriteWidth = spriteWidth;
            this.spriteHeight = spriteHeight;
            this.textureWidth = textureWidth;
            this.textureHeight = textureHeight;
            this.animSpeed = animSpeed;

            int columns = textureWidth / spriteWidth;

            //Dictionary mit einer Liste von Rechtecken fuellen.
            //Zeilenweise durchgehen. Muss natuerlich dem Spritesheet passend ubergeben werden.
            //Bspw. Up, Left, Right, Down.
            for (int i = 0; i < directions.Count; i++)
            {
                List<Rectangle> rects = new List<Rectangle>();
                //Nun durch die Zeileng gehen.
                for (int x = 0; x < columns; x++)
                {
                    rects.Add(new Rectangle(x * spriteWidth, i * spriteHeight, spriteWidth, spriteHeight));
                }
                spriteRectangles.Add(directions[i], rects);
            }

            animationRectangle = spriteRectangles[AnimationDirection.DOWN][0];
        }

        public Rectangle NextAnimation(GameTime gameTime, AnimationDirection direction, AnimationDirection previousDirection)
        {
            double currentTime = gameTime.TotalGameTime.TotalMilliseconds;

            //Gerade wird sich nicht bewegt, gucken ob wir vorher schonmal in einer Richtung schauten.
            //Falls nicht, setze die Standardrichtung.
            if (direction == AnimationDirection.NONE)
            {
                if (previousDirection != AnimationDirection.NONE)
                {
                    animationRectangle = spriteRectangles[previousDirection][0];
                }
                else
                {
                    animationRectangle = spriteRectangles[AnimationDirection.DOWN][0];
                }
                lastAnimTime = currentTime;
                return animationRectangle;
            }
            else
            {
                //Befanden wir uns vorher schon in der Richtung?
                //Falls nicht, faengt eine neue Animation an.
                if (direction != previousDirection)
                {
                    //Schauen wir schon in die Richtung? Bspw. beim Stehenbleiben?
                    if (animationRectangle == spriteRectangles[direction][0])
                    {
                        animFrame = 1;
                    }
                    else
                    {
                        animFrame = 0;
                    }
                }
                else //Hier ist der normale Animationsfluss.
                {

                    if ((currentTime - lastAnimTime) >= animSpeed)
                    {
                        if (animFrame == (spriteRectangles[direction].Count - 1))
                        {
                            animFrame = 0;
                        }
                        else
                        {
                            animFrame += 1;
                        }
                        lastAnimTime = currentTime;
                    }
                }
                animationRectangle = spriteRectangles[direction][animFrame];
                return animationRectangle;
            }
        }

        public static AnimationDirection KeyToDirection(Keys key)
        {
            switch (key)
            {
                case Keys.W:
                    return AnimationDirection.UP;
                case Keys.S:
                    return AnimationDirection.DOWN;
                case Keys.A:
                    return AnimationDirection.LEFT;
                case Keys.D:
                    return AnimationDirection.RIGHT;
                default:
                    return AnimationDirection.NONE;
            }
        }

    }

    public class SpinningAnimation : IAnimation
    {
        private int spriteWidth;
        private int spriteHeight;
        private int textureWidth;
        private int textureHeight;

        private int animSpeed;
        private int animFrame = 0;
        private double lastAnimTime = 0;

        //private Rectangle animationRectangle = new Rectangle();

        private Rectangle[] spinningFrames = new Rectangle[4];

        public SpinningAnimation(int spriteWidth, int spriteHeight, int textureWidth, int textureHeight, List<AnimationDirection> directions, int animSpeed = 200)
        {
            this.spriteWidth = spriteWidth;
            this.spriteHeight = spriteHeight;
            this.textureWidth = textureWidth;
            this.textureHeight = textureHeight;
            this.animSpeed = animSpeed;

            for(int i = 0; i < directions.Count; i++)
            {
                switch (directions[i])
                {
                    case AnimationDirection.DOWN:
                        spinningFrames[0] = new Rectangle(0, i * spriteHeight, spriteWidth, spriteHeight);
                        break;
                    case AnimationDirection.LEFT:
                        spinningFrames[1] = new Rectangle(0, i * spriteHeight, spriteWidth, spriteHeight);
                        break;
                    case AnimationDirection.UP:
                        spinningFrames[2] = new Rectangle(0, i * spriteHeight, spriteWidth, spriteHeight); 
                        break;
                    case AnimationDirection.RIGHT:
                        spinningFrames[3] = new Rectangle(0, i * spriteHeight, spriteWidth, spriteHeight); 
                        break;
                }
            }
        }

        public Rectangle NextAnimation(GameTime gameTime, AnimationDirection direction, AnimationDirection previousDirection)
        {
            double currentTime = gameTime.TotalGameTime.TotalMilliseconds;
            if (currentTime - lastAnimTime >= animSpeed)
            {
                if (this.animFrame == spinningFrames.Length - 1)
                {
                    this.animFrame = 0;
                }
                else
                {
                    this.animFrame += 1;
                }
                lastAnimTime = currentTime;
            }
            return spinningFrames[this.animFrame];
        }
    }


}
