﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TileEngine2D
{
    public interface IAnimation
    {
        Rectangle NextAnimation(GameTime gameTime, AnimationDirection direction, AnimationDirection previousDirection);
    }
}
