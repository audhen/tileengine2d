﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public interface IDrawable
    {
        int ZLevel
        {
            get;
        }
        Rectangle Hitbox
        {
            get;
        }
        void Draw(SpriteBatch spriteBatch);
    }
}
