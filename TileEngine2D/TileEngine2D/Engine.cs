using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace TileEngine2D
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Engine : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private SpriteBatch staticSpriteBatch;
        private String exeLoc = String.Empty;
        private DebugUI debugUI;
        private long updateCalls = 0;
        private long drawCalls = 0;
        public static bool DEBUGMODE = false;
        public static Texture2D DEBUGTEXTURE;
        public Camera camera;

        private List<IDrawable> uiElements = new List<IDrawable>();

        private List<Entity> npcs = new List<Entity>();
        private List<IDrawable> drawables = new List<IDrawable>();
        private List<Prop> props = new List<Prop>();

        public static Point RESOLUTION = new Point(800, 600);
        public static int CAMERA_WIDTH = 800;
        public static int CAMERA_HEIGHT = 600;
        public static Matrix CAMERA_MATRIX;



        public Map map;
        public Player player;
        public PlayerHealthbar pHealthbar;

        public Engine()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = RESOLUTION.X;
            graphics.PreferredBackBufferHeight = RESOLUTION.Y;

            TargetElapsedTime = new TimeSpan(0, 0, 0, 0, 33); //30FPS
            Content.RootDirectory = "Content";
            exeLoc = System.Reflection.Assembly.GetExecutingAssembly().Location;
            exeLoc = exeLoc.Replace(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe", "");
            debugUI = DebugUI.Instance;
            
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            
            Engine.DEBUGTEXTURE = Content.Load<Texture2D>("Debug/DEBUG");
            this.map = new Map(Content);
            //this.map = new Map(exeLoc + "DebugLevel.xml", Content);
            this.player = new Player(Content, this, new Vector2(400, 400), true);
            this.pHealthbar = new PlayerHealthbar(Content, new Vector2((Engine.CAMERA_WIDTH / 2) - 60, 5), 100, 100, 1f);
            this.uiElements.Add(pHealthbar);

            this.camera = new Camera(player, map);
            this.Npcs.Add(new Entity(Content, "Sprites/NPC/Isaac", new Vector2(200, 200), true));
            this.Npcs.Add(new SpinningEntity(Content, "Sprites/NPC/steampunk_m1", new Vector2(100, 290)));

            this.props.Add(new Prop(new Vector2(120, 240), Content.Load<Texture2D>("Sprites/Props/Schild"), true, true));
            this.props.Add(new Prop(new Vector2(320, 240), Content.Load<Texture2D>("Sprites/Props/Zaun")));
            this.props.Add(new Prop(new Vector2(344, 240), Content.Load<Texture2D>("Sprites/Props/Zaun"), true, true));

            this.drawables.Add(this.player);
            this.drawables.AddRange(this.Npcs);
            this.drawables.AddRange(this.props);


            debugUI.SetFont(Content.Load<SpriteFont>("Fonts/Arial7"));

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            staticSpriteBatch = new SpriteBatch(GraphicsDevice);

            this.map.Save(exeLoc + "DebugLevel.xml");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this.updateCalls += 1;

            map.Update(gameTime);

            foreach (Entity ent in Npcs)
            {
                ent.Update(gameTime);
            }

            player.Update(gameTime);            

            //Zlevels ermitteln und neusortieren
            this.drawables.Sort(new ZLevelComparer());

            //DebugOutput
            if (Engine.DEBUGMODE)
            {
                if (gameTime.ElapsedGameTime.Milliseconds > 0) debugUI.SetField(DebugFields.FPS, "" + (1000 / gameTime.ElapsedGameTime.Milliseconds));
                debugUI.SetField(DebugFields.Time, DateTime.Now.ToShortTimeString());
                debugUI.SetField(DebugFields.RunningSlow, gameTime.IsRunningSlowly.ToString());
                debugUI.SetField(DebugFields.UpdateCalls, this.updateCalls.ToString());
                debugUI.SetField(DebugFields.DrawCalls, this.drawCalls.ToString());
                debugUI.SetField(DebugFields.CallDifference, (-2 + this.updateCalls - this.drawCalls).ToString());
            }
            //camera.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            this.drawCalls += 1;
            GraphicsDevice.Clear(Color.Black);

            //spriteBatch.Begin();
            staticSpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, CAMERA_MATRIX);

            //Karte zeichnen.
            map.Draw(spriteBatch, new Rectangle((int)camera.Position.X, (int)camera.Position.Y, CAMERA_WIDTH, CAMERA_HEIGHT));

            foreach (IDrawable ent in this.drawables)
            {
                ent.Draw(spriteBatch);
                if (Engine.DEBUGMODE)
                {
                    spriteBatch.Draw(Engine.DEBUGTEXTURE, ent.Hitbox, Color.White);
                }
            }

            //UI-Overlay zeichnen
            foreach (IDrawable d in this.uiElements)
            {
                d.Draw(staticSpriteBatch);
            }

            //DebugOutput
            if (Engine.DEBUGMODE)
            {
                debugUI.Draw(staticSpriteBatch);
            }
            spriteBatch.End();
            staticSpriteBatch.End();
            base.Draw(gameTime);
        }

        public List<Entity> Npcs
        {
            get { return npcs; }
            set { }
        }

        public List<Prop> Props
        {
            get { return props; }
            set { }
        }
    }
}
