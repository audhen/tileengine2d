﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TileEngine2D
{
    public class DebugUI
    {
        private static DebugUI instance;
        private SpriteFont sfDefault;
        private Dictionary<DebugFields, DebugField> fields = new Dictionary<DebugFields, DebugField>();

        private DebugUI()
        {
            this.fields = new Dictionary<DebugFields, DebugField>();
            this.fields.Add(DebugFields.FPS, new DebugField(new Vector2(2, 0)));
            this.fields.Add(DebugFields.Time, new DebugField(new Vector2(2, 12)));
            this.fields.Add(DebugFields.UpdateCalls, new DebugField(new Vector2(2, 24)));
            this.fields.Add(DebugFields.DrawCalls, new DebugField(new Vector2(2, 36)));
            this.fields.Add(DebugFields.CallDifference, new DebugField(new Vector2(2, 48)));
            this.fields.Add(DebugFields.RunningSlow, new DebugField(new Vector2(2, 60)));
            
        }

        public static DebugUI Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DebugUI();
                }
                return instance;
            }
        }

        public void SetField(DebugFields f, String s)
        {
            if (this.fields.ContainsKey(f)) this.fields[f].Content = s;
        }

        public void SetFont(SpriteFont font)
        {
            this.sfDefault = font;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (KeyValuePair<DebugFields, DebugField> pair in this.fields)
            {
                spriteBatch.DrawString(sfDefault, pair.Key.ToString() + ": " + pair.Value.Content, pair.Value.Position, new Color(255,255,255, 170));
                //String ausgabe = String.Format("{0,-20}: {1,20}", pair.Key.ToString(), pair.Value.Content);
                //spriteBatch.DrawString(sfDefault, ausgabe, pair.Value.Position, Color.White);
            }
        }


    }
}
