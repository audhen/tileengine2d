﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace TileEngine2D
{
    public class PlayerHealthbar : IDrawable
    {
        private Vector2 position;
        private Texture2D textureBackground;
        private Texture2D textureFullHealth;
        private Texture2D textureEmptyHealth;
        private Vector2 barPosition;
        private Color transparency;


        private float scalingFactor;
        private float opacityFactor;
        private float maxHealth;
        private float currentHealth;

        public PlayerHealthbar(ContentManager content, Vector2 position, float maxHealth, float currentHealth, float scalingFactor = 1f, float opacityFactor = 1f)
        {
            Position = position;
            MaxHealth = maxHealth;
            CurrentHealth = currentHealth;
            ScalingFactor = scalingFactor;
            OpacityFactor = opacityFactor;

            textureBackground = content.Load<Texture2D>("Sprites/UI/HUD/Healthbar_Background");
            textureFullHealth = content.Load<Texture2D>("Sprites/UI/HUD/Healthbar_Voll");
            textureEmptyHealth = content.Load<Texture2D>("Sprites/UI/HUD/Healthbar_Leer");
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            float barOffsetX = Position.X + (((textureBackground.Width - textureFullHealth.Width) / 2f) * ScalingFactor);
            float barOffsetY = Position.Y + (((textureBackground.Height - textureFullHealth.Height) / 2f) * ScalingFactor);
            this.barPosition = new Vector2(barOffsetX, barOffsetY);
            spriteBatch.Draw(textureBackground, new Rectangle((int)Position.X, (int)Position.Y, (int)(textureBackground.Width * scalingFactor), (int)(textureBackground.Height * scalingFactor)), Color.White * OpacityFactor);
            spriteBatch.Draw(textureEmptyHealth, new Rectangle((int)barPosition.X, (int)barPosition.Y, (int)(textureEmptyHealth.Width * scalingFactor), (int)(textureEmptyHealth.Height * scalingFactor)), Color.White * OpacityFactor);
            float fullHealthbarWidth = ((100f / MaxHealth) * CurrentHealth) * scalingFactor;
            spriteBatch.Draw(textureFullHealth, new Rectangle((int)barPosition.X, (int)barPosition.Y, (int)fullHealthbarWidth, (int)(textureFullHealth.Height * scalingFactor)), Color.White * OpacityFactor);
        }


        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public int ZLevel
        {
            get { return (int)Position.Y; }
        }

        /// <summary>
        /// Nicht implementiert!
        /// Liefert nur ein neues Rechteck!
        /// </summary>
        public Rectangle Hitbox
        {
            get { return new Rectangle(); }
        }

        

        public float ScalingFactor
        {
            get { return scalingFactor; }
            set { scalingFactor = MathHelper.Clamp(value, 0.5f, 1f); }
        }


        public float MaxHealth
        {
            get { return maxHealth; }
            set { maxHealth = value; }
        }


        public float CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = MathHelper.Clamp(value, 0.5f, MaxHealth); }
        }

        public float OpacityFactor
        {
            get { return opacityFactor; }
            set { opacityFactor = MathHelper.Clamp(value, 0.3f, 1f); }
        }



    }
}
