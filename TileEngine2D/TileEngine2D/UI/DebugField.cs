﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TileEngine2D
{
    public class DebugField
    {
        private Vector2 position;
        private String content;

        public DebugField(Vector2 position, String content = "")
        {
            Position = position;
            Content = content;
        }

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public String Content
        {
            get { return content; }
            set { content = value; }
        }
    }

    public enum DebugFields
    {
        Time,
        FPS,
        FramesDrawn,
        EntitiesInSight,
        Collidables,
        DetectedCollisions,
        ViewPortDimension,
        PlayerCoords,
        RunningSlow,
        UpdateCalls,
        CallDifference,
        DrawCalls
    }
}